#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl.nginx > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'

